(function (window) {
    'use strict';
    
    // Get DOM element by CSS like selector
    window.$ = function(id){
        return document.querySelector(id);
    }
    
    // AJAX for search on wiki
    window.$.search = (function (searchKey, callback){
        // wiki url
        var wikiUrl = 'https://community-wikipedia.p.mashape.com/api.php' + 
            '?action=opensearch&search='+searchKey+'&format=json&limit=20';
        // wiki key
        var wikiKey = 'oCdV3MjLj1mshtyIXwBVzBqRKtY9p1XJNiajsn1vsCETYVLwK3';

        // process the request
        var req = new XMLHttpRequest();
        req.open('GET', wikiUrl, true);
        req.setRequestHeader('X-Mashape-Key', wikiKey);
        req.send();

        // catch the response
        req.onreadystatechange = function(){
            if(req.readyState == 4 && req.status == 200){
                // callback
                callback(req.responseText);
            }
        }
    });
    
    // build the wiki suggest api
    window.$.build = (function(domId_txt, domId_output, domId_loading, domId_btn){
        var listener = function(){
            // show loading img
            $(domId_loading).style.visibility= "visible";
            // promise for request
            var request = new Promise(function(resolve, reject){
                var searchKey = $(domId_txt).value;
                $.search(searchKey, function(response){
                    resolve({
                        results: JSON.parse(response), 
                        display: domId_output
                    });
                });
            });
            
            // after the request done
            request.then(function(bundle){                
                /* wiki search response json structure
                    [0] = search key, [1] = titles, [2] = intro of titles, [3] = url of titles */ 
                var domId_output = bundle.display;
                var titleList = bundle.results[1];
                var urlList = bundle.results[3];
                // remove the list (if exist) in the display box and create the order list
                $(domId_output).innerHTML = "";
                if(titleList.length > 0){
                    var ul_titles = document.createElement("UL");
                    document.getElementById("suggestion_display").appendChild(ul_titles);
                    // add items into the list
                    for(var i = 0; i < titleList.length; i++) {
                        var li_item = document.createElement('LI');
                        li_item.innerHTML = titleList[i];
                        var li_item_listener = function (url) {
                            return function(){
                                window.location = url;
                            };
                        };
                        li_item.addEventListener('click', li_item_listener(urlList[i]));
                        ul_titles.appendChild(li_item);
                    }

                    // add list into display box
                    $(domId_output).appendChild(ul_titles);
                }
                $(domId_loading).style.visibility= "hidden";
            });
            
        }
        if (domId_btn != null){
            $(domId_btn).addEventListener('click', listener);
        } else {
            $(domId_txt).addEventListener('keyup', listener);
            $(domId_txt).addEventListener('paste', listener);
        }
    });
    
    // setup interface (button click with loading img)
    window.$.setUp = (function(domId_txt, domId_output, domId_loading, domId_btn){
        $.build(domId_txt, domId_output, domId_loading, domId_btn);
    });
    // setup interface (text change with loading img)
    window.$.setUp = (function(domId_txt, domId_output, domId_loading){
        $.build(domId_txt, domId_output, domId_loading, null);
    });    
})(window);